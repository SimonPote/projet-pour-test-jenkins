package model.persistence;

import java.util.Date;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.apache.naming.java.javaURLContextFactory;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Advice {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private int note;
	@DateTimeFormat(pattern="DD/MM/YYYY")
	private Date date = new java.util.Date();
	private String comment;
	
	@ManyToOne
	private Article article;
	
	
	public Article getArticle() {
		return article;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public void setArticle(Article article) {
		this.article = article;
		
	}
	
	
}
