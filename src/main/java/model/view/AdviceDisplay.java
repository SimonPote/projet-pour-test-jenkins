package model.view;

import java.util.Date;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import model.persistence.Advice;


public class AdviceDisplay {
	

	private long id;
	private int note;
	@DateTimeFormat(pattern="DD/MM/YYYY")
	private Date date;
	private String comment;
	

	public AdviceDisplay(Advice advice) {
		this.id = advice.getId();
		this.note = advice.getNote();
		this.date = advice.getDate();
		this.comment = advice.getComment();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	
	
}
