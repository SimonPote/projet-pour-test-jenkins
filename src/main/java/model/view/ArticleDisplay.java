package model.view;

import model.persistence.Advice;
import model.persistence.Article;


public class ArticleDisplay {
	

	private long id;
	private String name;
	private float price;
	private float noteMoy;
	
	public ArticleDisplay(long id, String name, float price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}
	public ArticleDisplay(Article article) {
		float sumNote = 0;
		this.id = article.getId();
		this.name = article.getName();
		this.price = article.getPrice();
		for (Advice advice : article.getListAdvices()) {
			sumNote += advice.getNote();
		}
		this.noteMoy = (float) sumNote/article.getListAdvices().size();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float getNoteMoy() {
		return noteMoy;
	}
	public void setNoteMoy(float noteMoy) {
		this.noteMoy = noteMoy;
	}
	
}
