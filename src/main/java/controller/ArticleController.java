package controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import model.persistence.Article;
import model.persistence.Message;
import model.view.ArticleDisplay;
import repository.ArticleRepository;

@RestController
@Configuration
@ComponentScan
public class ArticleController {

	@Autowired 
	ArticleRepository articleRepo;

	@RequestMapping(path="/createArticle", method=RequestMethod.POST)
	public void createArticle(Article a) {
		articleRepo.save(a);
	}

	@RequestMapping(path="/articles", method=RequestMethod.GET)
	public ResponseEntity<List<ArticleDisplay>> displayArticles(
			@RequestParam(value="sortBy", required=false, defaultValue="price") String sortBy, 
			@RequestParam(value="order", required=false, defaultValue="asc") String order) {
		List<ArticleDisplay> listToDisplay = new ArrayList<>();
		switch (sortBy) {
		case "name":
			for (Article article : articleRepo.findAllByOrderByNameAsc()) {
				listToDisplay.add(new ArticleDisplay(article));
			}
			break;
		case "id":
			for (Article article : articleRepo.findAllByOrderByIdAsc()) {
				listToDisplay.add(new ArticleDisplay(article));
			}
			break;
		case "notemoy":
			for (Article article : articleRepo.findAll()) {
				listToDisplay.add(new ArticleDisplay(article));
			}
			listToDisplay.sort(new ComparatorByNoteMoy());
			break;
		default:
			for (Article article : articleRepo.findAllByOrderByPriceAsc()) {
				listToDisplay.add(new ArticleDisplay(article));
			}
			break;
		}
		switch (order) {
		case "desc":
			Collections.reverse(listToDisplay);
			break;
		default:
		}
		return new ResponseEntity<List<ArticleDisplay>>(listToDisplay, HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(path="/articles2", method=RequestMethod.GET)
	public ResponseEntity<List<ArticleDisplay>> displayArticles2(
			@RequestParam(value="sortBy", required=false, defaultValue="price") String sortBy, 
			@RequestParam(value="order", required=false, defaultValue="Asc") String order) {
		List<ArticleDisplay> listToDisplay = new ArrayList<>();
		for (Article article : articleRepo.findAll(new Sort(Sort.Direction.fromString(order), sortBy))) {
				listToDisplay.add(new ArticleDisplay(article));
			}
//		switch (order) {
//		case "desc":
//			Collections.reverse(listToDisplay);
//			break;
//		default:
//			break;
//		}
		return new ResponseEntity<List<ArticleDisplay>>(listToDisplay, HttpStatus.ACCEPTED);
	}


	@RequestMapping(path="/article/{id}")
	public ResponseEntity<ArticleDisplay> displayArticle(@PathVariable("id") long id) {
		try {
			ArticleDisplay articleDisplay = new ArticleDisplay(articleRepo.getOne(id));
			return new ResponseEntity<ArticleDisplay>(articleDisplay, HttpStatus.ACCEPTED);
		}
		catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<ArticleDisplay>(HttpStatus.NOT_FOUND);
		}
	}

	
	@RequestMapping(path="/article/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Message> deleteArticle(@PathVariable("id") long id) {
		try {
			articleRepo.deleteById(id);
			Message message = new Message();
			message.setText("Votre article a bien été supprimé.");
			return new ResponseEntity<Message>(message, HttpStatus.ACCEPTED);

		}catch (Exception e) {
			Message message = new Message();
			message.setText("L'article n'a pas été trouvé. Id incorrecte");
			return new ResponseEntity<Message>(message, HttpStatus.NOT_FOUND);

		}

	}


}
