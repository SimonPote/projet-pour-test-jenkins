package controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import model.persistence.Advice;
import model.persistence.Article;
import model.persistence.Message;
import model.view.AdviceDisplay;
import repository.AdviceRepository;
import repository.ArticleRepository;

@RestController
@Configuration
@ComponentScan
public class AdviceController {

	@Autowired 
	AdviceRepository adviceRepo;
	@Autowired 
	ArticleRepository articleRepo;

	@RequestMapping(path="/article/{id}/advice", method=RequestMethod.POST)
	public ResponseEntity<Message> createAdvice(Advice advice, @PathVariable("id") long id) {
		try {
			Article article = articleRepo.getOne(id);
			if (advice.getNote() < 1 || advice.getNote() > 5) {
				Message message = new Message();
				message.setText("Votre advice n'a pas été créé, note incorrecte.");
				return new ResponseEntity<Message>(message, HttpStatus.BAD_REQUEST);
			}
			else {
				advice.setArticle(article);
				adviceRepo.save(advice);
				Message message = new Message();
				message.setText("Votre advice a bien été créé.");
				return new ResponseEntity<Message>(message, HttpStatus.CREATED);
			}
		}
		catch (javax.persistence.EntityNotFoundException e) {
			Message message = new Message();
			message.setText("Votre advice n'a pas été créé. Id d'article incorrecte");
			return new ResponseEntity<Message>(message, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path="/article/{id}/advices", method=RequestMethod.GET)
	public ResponseEntity displayAdvices(
			@PathVariable("id") long id,
			@RequestParam(value="comment", required=false, defaultValue="false") String comment) {
		List<AdviceDisplay> listAdviceToDisplay = new ArrayList<>();
		try {
			switch (comment ) {
			case "true":
				for (Advice advice : adviceRepo.findByArticleAndCommentNotNull(articleRepo.getOne(id))) {
					listAdviceToDisplay.add(new AdviceDisplay(advice));
				}
				break;
			default:
				for (Advice advice : adviceRepo.findAllByArticle(articleRepo.getOne(id))) {
					listAdviceToDisplay.add(new AdviceDisplay(advice));
				}
				break;
			}
			return new ResponseEntity<List<AdviceDisplay>>(listAdviceToDisplay, HttpStatus.ACCEPTED);
		}
		catch (javax.persistence.EntityNotFoundException e) {
			Message message = new Message();
			message.setText("Votre advice n'a pas été créé. Id d'article incorrecte");
			return new ResponseEntity<Message>(message, HttpStatus.NOT_FOUND);
		}
	}
	
	//	@RequestMapping(path="/advice/{id}")
	//	public ResponseEntity displayAdvice(@PathVariable("id") long id) {
	//		for (Article article : ArticleController.listArticles) {
	//			for (Advice advice : article.getListAdvices()) {
	//				if (advice.getId() == id) return new ResponseEntity<Advice>(advice, HttpStatus.ACCEPTED);
	//			}
	//		}
	//		Message message = new Message();
	//		message.setText("id incorrecte.");
	//		return new ResponseEntity<Message>(message, HttpStatus.NOT_FOUND);
	//	}
	//
	//	@RequestMapping(path="/advice/{id}", method=RequestMethod.DELETE)
	//	public ResponseEntity<Message> deleteAdvice(@PathVariable("id") long id) {
	//		for (Article article : ArticleController.listArticles) {
	//			for (Advice advice : article.getListAdvices()) {
	//				if (advice.getId() == id) {
	//					article.getListAdvices().remove(advice);
	//					Message message = new Message();
	//					message.setText("Votre article a bien été supprimé.");
	//					return new ResponseEntity<Message>(message, HttpStatus.ACCEPTED);
	//				}
	//			}
	//		}
	//		Message message = new Message();
	//		message.setText("id incorrecte.");
	//		return new ResponseEntity<Message>(message, HttpStatus.NOT_FOUND);
	//	}
}
