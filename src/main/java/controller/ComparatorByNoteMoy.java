package controller;

import java.util.Comparator;

import model.persistence.Article;
import model.view.ArticleDisplay;

public class ComparatorByNoteMoy implements Comparator<ArticleDisplay> {

	@Override
	public int compare(ArticleDisplay o1, ArticleDisplay o2) {
		return Float.compare(o1.getNoteMoy(),o2.getNoteMoy());
	}

}
