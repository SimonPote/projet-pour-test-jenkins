package repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import model.persistence.Article;
import org.springframework.data.domain.Sort;

public interface ArticleRepository extends JpaRepository<Article, Long> {

	@Query(value = "SELECT avg(note) from Advice where article = ?1", nativeQuery = true)
	float getAverage(Article article);
	
	List<Article> findAllByOrderByIdAsc();

	List<Article> findAllByOrderByNameAsc();

	List<Article> findAllByOrderByPriceAsc();

	List<Article> findAll(Sort sort);
	
}
