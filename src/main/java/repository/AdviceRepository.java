package repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import model.persistence.Advice;
import model.persistence.Article;

public interface AdviceRepository extends JpaRepository<Advice, Long> {

	List<Advice> findAllByArticle(Article article);

	//List<Advice> findByArticleByCommentNotNull(Article one);

	List<Advice> findByArticleAndCommentNotNull(Article one);

}
